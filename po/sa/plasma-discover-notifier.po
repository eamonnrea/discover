# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the discover package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-08-18 00:40+0000\n"
"PO-Revision-Date: 2023-10-01 00:23+0530\n"
"Last-Translator: \n"
"Language-Team: Sanskrit\n"
"Language: sa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n>2);\n"
"X-Generator: Poedit 3.3.2\n"
"X-Poedit-Bookmarks: -1,-1,15,-1,-1,-1,-1,-1,-1,-1\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: notifier/DiscoverNotifier.cpp:176
#, kde-format
msgid "View Updates"
msgstr "अद्यतनं पश्यन्तु"

#: notifier/DiscoverNotifier.cpp:288
#, kde-format
msgid "Security updates available"
msgstr "सुरक्षा अद्यतनं उपलब्धम्"

#: notifier/DiscoverNotifier.cpp:290
#, kde-format
msgid "Updates available"
msgstr "अद्यतनं उपलब्धम्"

#: notifier/DiscoverNotifier.cpp:292
#, kde-format
msgid "System up to date"
msgstr "तन्त्रं अद्यतनम् अस्ति"

#: notifier/DiscoverNotifier.cpp:294
#, kde-format
msgid "Computer needs to restart"
msgstr "सङ्गणकस्य पुनः आरम्भस्य आवश्यकता वर्तते"

#: notifier/DiscoverNotifier.cpp:296
#, kde-format
msgid "Offline"
msgstr "वियुक्त"

#: notifier/DiscoverNotifier.cpp:298
#, kde-format
msgid "Applying unattended updates…"
msgstr "अप्रयुक्तानि अद्यतनं प्रयोजयन्…"

#: notifier/DiscoverNotifier.cpp:323
#, kde-format
msgid "Restart is required"
msgstr "पुनर्प्रारम्भः आवश्यकः अस्ति"

#: notifier/DiscoverNotifier.cpp:324
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr "अद्यतनस्य प्रभावाय तन्त्रस्य पुनः आरम्भस्य आवश्यकता वर्तते।."

#: notifier/DiscoverNotifier.cpp:329
#, fuzzy, kde-format
#| msgid "Computer needs to restart"
msgctxt "@action:button"
msgid "Update and Restart"
msgstr "सङ्गणकस्य पुनः आरम्भस्य आवश्यकता वर्तते"

#: notifier/DiscoverNotifier.cpp:330
#, kde-format
msgctxt "@action:button"
msgid "Update and Shut Down"
msgstr ""

#: notifier/DiscoverNotifier.cpp:374
#, kde-format
msgid "Upgrade available"
msgstr "उन्नयनं उपलब्धम्"

#: notifier/DiscoverNotifier.cpp:375
#, kde-format
msgctxt "A new distro release (name and version) is available for upgrade"
msgid "%1 is now available."
msgstr "%1 इदानीं उपलभ्यते ।."

#: notifier/DiscoverNotifier.cpp:378
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "उन्नयनं कुर्वन्तु"

#: notifier/main.cpp:38
#, kde-format
msgid "Discover Notifier"
msgstr "डिस्कवर् नोटिफायर्"

#: notifier/main.cpp:40
#, kde-format
msgid "System update status notifier"
msgstr "तन्त्र अद्यतन स्थिति सूचक"

#: notifier/main.cpp:42
#, fuzzy, kde-format
#| msgid "© 2010-2022 Plasma Development Team"
msgid "© 2010-2024 Plasma Development Team"
msgstr "© २०१०-२०२२ प्लाज्मा विकासदल"

#: notifier/main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "श्रीकान्त् कलवार्"

#: notifier/main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "skkalwar999@gmail.com"

#: notifier/main.cpp:51
#, kde-format
msgid "Replace an existing instance"
msgstr "विद्यमानं दृष्टान्तं प्रतिस्थापयन्तु"

#: notifier/main.cpp:53
#, kde-format
msgid "Do not show the notifier"
msgstr "सूचनादाता न दर्शयतु"

#: notifier/main.cpp:53
#, kde-format
msgid "hidden"
msgstr "अदृष्ट"

#: notifier/NotifierItem.cpp:21 notifier/NotifierItem.cpp:22
#, kde-format
msgid "Updates"
msgstr "अद्यतनाः"

#: notifier/NotifierItem.cpp:34
#, kde-format
msgid "Open Discover…"
msgstr "डिस्कवरं उद्घाटयन्तु…"

#: notifier/NotifierItem.cpp:39
#, kde-format
msgid "See Updates…"
msgstr "अद्यतनं पश्यन्तु…"

#: notifier/NotifierItem.cpp:44
#, kde-format
msgid "Refresh…"
msgstr "नवीकरणम्…"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Install Updates and Restart…"
msgstr ""

#: notifier/NotifierItem.cpp:53
#, kde-format
msgid "Install Updates and Shut Down…"
msgstr ""

#: notifier/NotifierItem.cpp:58
#, kde-format
msgid "Restart to apply installed updates"
msgstr "संस्थापितानि अद्यतनानि प्रयोक्तुं पुनः आरभत"

#: notifier/NotifierItem.cpp:59
#, kde-format
msgid "Click to restart the device"
msgstr "यन्त्रं पुनः आरभ्यतुं नोदनम् कुर्वन्तु"

#~ msgctxt "@action:button"
#~ msgid "Restart"
#~ msgstr "पुनः आरभत"
